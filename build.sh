#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x$SELECTEDOPTION = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x$SELECTEDOPTION = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x$SELECTEDOPTION = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo "Optimization level $OPTIMIZATION is not yet implemented!"
        exit 1
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( OGGDIR="${PWD}/../library-ogg/${MULTIARCHNAME}"
      VORBISDIR="${PWD}/../library-vorbis/${MULTIARCHNAME}"
      cd "$BUILDDIR"
      ../source/configure \
        --prefix="$PREFIXDIR" \
        --disable-static \
        --disable-encode \
        --disable-examples \
        --with-ogg="$OGGDIR" \
        --with-vorbis="$VORBISDIR"
      make
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig,share} )

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

darwin_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-g"
        export CXXFLAGS="-g"
    fi

    case "$MULTIARCHNAME" in
    i386-darwin-macos)
        export CFLAGS="$CFLAGS -arch i386"
        export CXXFLAGS="$CXXFLAGS -arch i386"
        export LDFLAGS="$LDFLAGS -arch i386"
        ;;
    x86_64-darwin-macos)
        export CFLAGS="$CFLAGS -arch x86_64"
        export CXXFLAGS="$CXXFLAGS -arch x86_64"
        export LDFLAGS="$LDFLAGS -arch x86_64"
        ;;
    universal-darwin-macos)
        export CFLAGS="$CFLAGS -arch i386 -arch x86_64"
        export CXXFLAGS="$CXXFLAGS -arch i386 -arch x86_64"
        export LDFLAGS="$LDFLAGS -arch i386 -arch x86_64"
        ;;
    *)
        echo "$MULTIARCHNAME is not (yet) supported by this script."
        exit 1;;
    esac

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"
    export MACOSX_DEPLOYMENT_TARGET=10.7
    # ( cd source
    #   ./autogen.sh)

    ( OGGDIR="${PWD}/../library-ogg/${MULTIARCHNAME}"
      VORBISDIR="${PWD}/../library-vorbis/${MULTIARCHNAME}"
      cd "$BUILDDIR"
      export OGG_CFLAGS="-I${OGGDIR}/include"
      export OGG_LIBS="-L${OGGDIR}/lib -logg"
      export VORBIS_CFLAGS="-I${OGGDIR}/include"
      export VORBIS_LIBS="-L${OGGDIR}/lib -logg"
      ../source/configure \
        --prefix="$PREFIXDIR" \
        --with-ogg="$OGGDIR" \
        --with-vorbis="$VORBISDIR" \
        --disable-vorbistest \
        --disable-examples \
        --disable-oggtest \
        --disable-sdltest \
        --enable-docs=no \
        --disable-encode \
        --disable-static
      make -j`getconf _NPROCESSORS_ONLN`
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig,share}
      (cd "$PREFIXDIR"
          for n in  lib/lib*.*.dylib; do
              echo "Fixing name of  of $n"
              install_name_tool -id @executable_path/../Frameworks/$n $n
              install_name_tool -change $PREFIXDIR/lib/libvorbis.0.dylib @executable_path/../Frameworks/libvorbis.0.dylib $n
          done
      )
    )

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
i386-darwin-macos)
    darwin_build;;
x86_64-darwin-macos)
    darwin_build;;
universal-darwin-macos)
    darwin_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-theora.txt

rm -rf "$BUILDDIR"

echo "Theora for $MULTIARCHNAME is ready."
